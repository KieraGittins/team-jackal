﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyBulletOnCol : MonoBehaviour
{

    private void OnCollisionEnter(Collision col)
    {
        if (!(col.gameObject.tag == "Player"))
        {
            Destroy(gameObject);
        }
        
    }

}
