﻿using UnityEngine;
using System.Collections;

public class ShootScript : MonoBehaviour 
{
    public Transform bulletSpawn;
    public GameObject shootPrefab;
	public Transform headDirection;
	public Vector3 objectRotation;
	public float shootForce = 30f;

	public AudioClip shootSound;
	private AudioSource shootSource;
	public GameObject shootParticle;
	private ParticleSystem shootSystem;
    public float shootVolume = 0.4f;

	void Awake()
	{
		shootSource = this.gameObject.AddComponent<AudioSource>();
		shootSource.loop = false;
		shootSource.playOnAwake = false;
        shootSource.volume = shootVolume;
		if(shootSound != null)
			shootSource.clip = shootSound;

	}

	void Update () 
	{
		if(Input.GetButtonDown("Fire1"))
		{
			
			//instatiates the shootPrefab, sets its position/rotation and stores its rigidbody
			GameObject projectile = Instantiate(shootPrefab, bulletSpawn.position, bulletSpawn.rotation);

			//adds force to the objecct
			if(projectile.GetComponent<Rigidbody>() != null)
			{
				Rigidbody projectileRigidbody = projectile.GetComponent<Rigidbody>();
				projectileRigidbody.AddForce(headDirection.transform.forward * shootForce, ForceMode.Impulse);

                if (shootParticle != null)
                {
                    shootSystem = ((GameObject)Instantiate(shootParticle, bulletSpawn.position, bulletSpawn.rotation)).GetComponent<ParticleSystem>();
                    Destroy((GameObject)shootSystem.gameObject, shootSystem.main.duration);
                }
                if (shootSound != null)
                    shootSource.Play();
            }
			else 
				Debug.LogError("The gameobject you are trying to use does not have a rigidbody");
		}
	}
}
