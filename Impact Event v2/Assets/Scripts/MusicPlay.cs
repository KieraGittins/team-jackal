﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class MusicPlay : MonoBehaviour {

    public AudioClip bgMusicClip;
    public float Volume = 0.4f;
    private AudioSource audMusic;

    // Use this for initialization
    void Start () {

        audMusic = this.gameObject.AddComponent<AudioSource>();
        audMusic.loop = true;
        audMusic.playOnAwake = false;
        if (bgMusicClip != null)
            audMusic.clip = bgMusicClip;
        audMusic.volume = Volume;
        audMusic.Play();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
