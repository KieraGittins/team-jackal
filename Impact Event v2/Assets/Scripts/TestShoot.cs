﻿using UnityEngine;

public class TestShoot : MonoBehaviour
{
    public GameObject bulletPrefab;
    public Transform headDirection;
    public Vector3 objectRotation;
    public float shootForce = 30f;
    public Transform bulletSpawn;

    public AudioClip shootSound;
    private AudioSource shootSource;
    public GameObject shootParticle;
    private ParticleSystem shootSystem;
    

    void Awake()
    {
        shootSource = this.gameObject.AddComponent<AudioSource>();
        shootSource.loop = false;
        shootSource.playOnAwake = false;
        if (shootSound != null)
            shootSource.clip = shootSound;

    }

    void Update()
    {
        
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            Fire();
        }
    }


    void Fire()
    {
        // Create the Bullet from the Bullet Prefab
        var bullet = (GameObject)Instantiate(
            bulletPrefab,
            bulletSpawn.position,
            bulletSpawn.rotation);

        // Add velocity to the bullet
        bullet.GetComponent<Rigidbody>().velocity = bullet.transform.forward * 20;

        // Destroy the bullet after 2 seconds
        Destroy(bullet, 2.0f);        
    }
    
}